﻿/*
 * Created by SharpDevelop.
 * User: Hananel
 * Date: 28/08/2015
 * Time: 09:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace Archive_Brute_Force_Attack
{
	class Program
	{
		static readonly object _object = new object();
		static bool[] keep_run;
		
		// --- parameters
		static string ProtectedFile,SevenZipCMDPath;
		static string[] posibilitys ;
		static int Number_of_Posibilies = 0;
		static int[] Combination;
		static Thread[] thread;
		static int Number_Paralel_Program;
		static UInt64 Global_Counter = 0,continue_from =0;
		
		//--------------------------
		
		public static void worker(){
			
			var proc = new ProcessStartInfo();
			proc.WindowStyle = ProcessWindowStyle.Hidden;
			proc.CreateNoWindow = false;
			proc.UseShellExecute = false;
			proc.RedirectStandardOutput = true;
			proc.RedirectStandardError = true;
			proc.FileName = SevenZipCMDPath;
			
			int pid = Convert.ToInt32(Thread.CurrentThread.Name);
			
			string pass;
			Process x;
			int exit_code = 1;
			do{
				if (!keep_run[pid])
					return;
				
				// counter increment
				lock (_object)
				{
					int f = 0;
					for (int i = 0; i < Number_of_Posibilies; i++) {
						Combination[i]++;
						
						if (Combination[i] == Number_of_Posibilies){
							Combination[i] = 0;
							f++;
						}else{
							break;
						}
					}
					if (f == Number_of_Posibilies)
						for (int i = 0; i < Number_Paralel_Program; i++)
							keep_run[i] = false;
					
					pass="";
					for (int i = 0; i < Number_of_Posibilies; i++)
						if (Combination [i]>-1)
							pass+=posibilitys [Combination [i]];
					
					Global_Counter++;
					Console.WriteLine(Global_Counter.ToString()+". ) "+ pass);
				}
				
				if (Global_Counter < continue_from)
					continue;
				
				proc.Arguments = "l \"" + ProtectedFile + "\" -p"+pass;
				
				x = Process.Start(proc);
				x.BeginOutputReadLine();
				string stdout = x.StandardError.ReadToEnd();
				
				exit_code =x.ExitCode;
				
			}while(exit_code>0);
			
			for (int i = 0; i < Number_Paralel_Program; i++){
				if (pid==i) continue;
				keep_run[i] = false;
				thread[i].Join();
			}
			
			
			Console.Write("Congratulation, The password is : "+pass);
		}
		
		
		public static void Main(string[] args)
		{
			// --- Please set the parameters before run
			Number_Paralel_Program = 15;
			continue_from = 0;
			ProtectedFile = @"D:\Downloads\7z1506-x64\archive.7z";
			SevenZipCMDPath=@"D:\Downloads\7z1506-x64\7z.exe";
			posibilitys  = new string[]{
				"a",
				"b",
				"c",
				"d",
				"e",
				"f",
				"g",
				"h",
				"i",
				"j"
			};
			//--------------------------
			
						
			// --- init
			Number_of_Posibilies = posibilitys .Length;
			Combination = new int[Number_of_Posibilies];
			for (int i = 0; i < Number_of_Posibilies; i++) Combination [i] = -1;
			//--------------------------
			
			// --- Run
			thread = new Thread[Number_Paralel_Program];
			keep_run = new bool[Number_Paralel_Program];
			for (int p = 0; p < Number_Paralel_Program ; p++) {
				keep_run[p] = true;
				thread[p] = new Thread(worker);
				thread[p].Name = p.ToString();
				thread[p].Start();
			}
			
			Console.ReadKey(true);
		}
	}
}