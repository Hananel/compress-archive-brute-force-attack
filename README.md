# Did you forgot the password for your compressed file archive? #

This code can open by brute force any file format that 7zip support (including 7z, zip, rar, cab, iso, tgz, arj, gz, gzip).

To speed up the process you need to change some parameters before compiling the program. (see below)

### Requirements: ###

* Sharp-Dev or Visual Studio for editing and compiling the C# code.
* 7-Zip install on your system and the full path to 7z.exe (i.e. D:\Downloads\7z1506-x64\7z.exe).

### What you need to change: ###

The parameters that need to be set before the program can run locate at the beginning of the main function:

```
#!c#

// --- Please set the parameters before run
Number_Paralel_Program = 15;
continue_from = 0;
ProtectedFile = @"D:\archive.7z";
SevenZipCMDPath=@"D:\Downloads\7z1506-x64\7z.exe";
posibilitys = new string[]{
                "a",
                "b",
                "c",
                "d",
                "e",
                "f",
                "g",
                "h",
                "i",
                "j"
             };
//--------------------------

```

### The parameters: ###

* Number_Paralel_Program = should be set to the number of attempts that you want to done simultaneously and should not be more then the number of the cores that in your computer.
* continue_from = if you run the program for the first time it need to set to zero, if you wont to continue previews run its need to be set to the last attempt number (all other parameters must not change between the runs).
* ProtectedFile= set to the path of the protected archive
* SevenZipCMDPath = set the path to the 7z.exe program that installed on your system
* possibility = this array should be set to the least set of possible passwords. For example, you remember that the password to the file contain combination of characters like: 123 (or/and) abc
    There for the password can be one of the 6 possibilities (2^2+2)

    123
    abc

    123abc 
    abc123

    123abc
    abc123

    so possibility should be set like this

    
```
#!c#

posibilitys = new string[]{
                    "123",
                    "abc",
                 };
```


### Limitations: ###

The program use external binary file, it mean that it use the windows environment command prompt to run and try the archive file. therefore the program is not optimized for long brute force attack.